## b@b Enhancment - Growing enhancement suite for BoredAt sites

#### Scroll to last post
Keeps track of the last post seen, and allows you to scroll to it easily when you return
Currently, only keeps track of the top post on the page, regardless of whether you've seen it. This will probably not change.

#### Changed header links
Settings page now offers customization of links in header. Future releases may prevent the oferflow that occurs when there are too many links. No promises.

#### Confirm navigation
If you try to navigate away from a page with unsubmitted text, you will be asked for confirmation. A future release will provide the option to turn this off.

#### Show notification count in tab
Like Faceebook, now you can see how many notifications if you're doing something in another tab. Refreshes every 30 seconds, which may be customizable in the future.


Please note: Not all features work as intended in incognito mode.