$(window).load(function () {
	var path = window.location.pathname;
	var header = $('body').find('.headerLinks');
	var allHeaderLinks = {
		'Home': '/page',
		'Profile': '/profile',
		'Check-in': '/checkin',
		'Account': '/account',
		'Logout': '/actions/logout.php',
		'Notifications': '/notifications',
		'Heartbeat': '/heartbeat',
		'Global': '/global/page',
		'Local': '/page',
		'Last Post': '#" id="extScroll'
	};
	function retrieveWindowVariables(variables) {
		var ret = {};
		var currVariable;

		var scriptContent = '';
		for (var i = 0; i < variables.length; i++) {
			currVariable = variables[i];
			scriptContent += 'if (typeof ' + currVariable + ' !== "undefined") $("body").attr("tmp_' + currVariable + '", ' + currVariable + ');\n';
		}

		var script = document.createElement('script');
		script.id = 'tmpScript';
		script.appendChild(document.createTextNode(scriptContent));
		(document.body || document.head || document.documentElement).appendChild(script);

		for (var j = 0; j < variables.length; j++) {
			currVariable = variables[j];
			ret[currVariable] = $('body').attr('tmp_' + currVariable);
			$('body').removeAttr('tmp_' + currVariable);
		}

		$('#tmpScript').remove();

		return ret;
	}

	/*Other header links*/
	header.children().hide();
	var headerLinks = JSON.parse(localStorage.headerLinks || null);

	if (!headerLinks) {
		headerLinks = ['Home', 'Profile', 'Check-in', 'Account', 'Logout'];
	}

	var val;
	$.each(headerLinks, function(i, el) {
		val = allHeaderLinks[el];
		if (path.indexOf('global') >= 0) {
			if (el !== 'Global') val = '/global' + val;
		}
		if (el === 'Last Post' && path.indexOf('page') < 0) return true;
		header.append('<a href="' + val + '">' + el + '</a>');
	});


	if (path.indexOf('account') >= 0) {
		$('form#prefs > div.grid_9:nth-child(3) > .grid_8')
		.before('<div class="grid_4 bottom_border"><label>Header:</label></div>')
		.before('<div class="grid_4 bottom_border"><div class="grid_4"><label>Home</label><input type="checkbox" name="Home"><label>Profile</label><input type="checkbox" name="Profile"><label>Check-in</label><input type="checkbox" name="Check-in"><label>Account</label><input type="checkbox" name="Account"></div><div class="grid_4"><label>Logout</label><input type="checkbox" name="Logout"><label>Notifications</label><input type="checkbox" name="Notifications"><label>Heartbeat</label><input type="checkbox" name="Heartbeat"><label>Global</label><input type="checkbox" name="Global"></div><div class="grid_4"><label>Local</label><input type="checkbox" name="Local"><label>Last Seen Post</label><input type="checkbox" name="Last Post"></div></div>');

		$.each(headerLinks, function(i, el) {
			$('input[name="' + el + '"]')[0].checked = true;
		});
		$('input[value="Save Settings"]').click(function(e){
			headerLinks = [];
			$('input[type="checkbox"]:checked').each(function(i, el) {
				headerLinks.push( $(el).attr('name') );
			});
			localStorage.headerLinks =JSON.stringify( headerLinks );
		});
	}




	/*Scroll to*/
	if (path.indexOf('page') >= 0) {
		if (path.indexOf('global') >= 0) {
			topPostId = localStorage["glob@lid"];
		} else {
			topPostId = localStorage["b@bid"];
		}
		if (!topPostId) {
			topPostId = $('body').find('.post:first').attr('id').substr(4);
			console.warn("id undefined");
		}
		console.assert(topPostId, "Id not defined on Load");

		$('#extScroll').click(function () {
			var topPostOffset = $('#post' + topPostId).offset();
			if (topPostOffset) {
				$('html, body').animate({
					scrollTop: topPostOffset.top
				}, 1000);
			} else {
				window.location.href = "/actions/find_in_context.php?pid=" + topPostId;
			}
		});
	}


	/*Display notification count*/
	$('.headerLinks > a:contains("Notifications")').append( '<span id="notificationCount"></span>' );
	var pageTitle = document.title;

	setInterval(function(){
		var winVars = retrieveWindowVariables(['notifications']);
		var nots = winVars.notifications;

		if (nots > 0){
			header.find('#notificationCount').text('(' + nots + ')');
			document.title = '(' + nots + ') ' + pageTitle;
		} else {
			header.find('#notificationCount').text('');
			document.title = pageTitle;
		}
	}, 30000);

});


$(window).on("beforeunload", function( event ) {
	event.preventDefault();
	if (window.location.pathname.indexOf('page') >= 0) {
		topPost = $('body').find('.post:first');
		console.assert(topPost, "topPost not defined on unload");

		if (topPostId) {
			topPostId = Math.max(topPost.attr('id').substr(4), topPostId);
		}
		console.assert(topPostId, "Id not defined on unload");

		if (window.location.pathname.indexOf('global') >= 0) {
			localStorage["glob@lid"] = topPostId;
		} else {
			localStorage["b@bid"] = topPostId;
		}
	}

	var unsaved = false;
	$('textarea').each(function(i,el) {
		if ($(el).val()) {
			unsaved = true;
		}
	});
	if (unsaved) return "You have an unsbumitted post. Are you sure you want to leave?";
});
